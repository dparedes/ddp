<?php

class Session
{
    
    public static function init()
    {
        @session_start();
    }
    
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
		//echo "Valor 1: " .$key. ", Valor 2: " .$value;
		//die();
    }
    
    public static function get($key)
    {
        if (isset($_SESSION[$key]))
        return $_SESSION[$key];
    }
    
    public static function destroy()
    {
        //unset($_SESSION);
        session_destroy();
    }    
	
    
}