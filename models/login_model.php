<?php

class Login_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $sth = $this->db->prepare("SELECT * FROM usuarios_web WHERE 
                E_MAIL = :login AND CLAVE = :password");
        $sth->execute(array(
            ':login' => $_POST['login'],
            ':password' => $_POST['password']));
        
        $data = $sth->fetch();
        
        $count =  $sth->rowCount();
        if ($count > 0) {
            // login
            Session::init();
            Session::set('email', $data['E_MAIL']);
            Session::set('loggedIn', true);
            Session::set('userid', $data['N_USUARIO']);
            Session::set('name', $data['NOMBRE']);
            Session::set('accountId', $data['CUENTA_TARJ']);
            Session::set('companyId', $data['No_EMPRESA']);
            Session::set('lastLogin', $data['FECHA_ULT_ENTRADA']);
            header('location: ../dashboard');
        } else {
			Session::init();
			Session::set('error', 'error');
            header('location: ../login');
			
        }
        
    }
    
}