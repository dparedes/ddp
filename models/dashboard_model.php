<?php

class Dashboard_Model extends Model {

    public function __construct() {
        parent::__construct();
    }
    
        public function AccountDetail($accountId, $companyId)
    {
        return $this->db->select("SELECT * FROM saldos_web WHERE numero_cuenta = :accountId AND numero_de_empresa  = :companyId",
        array('accountId' => $accountId,
               'companyId' => $companyId));
    }
        public function LastMovements($accountId, $companyId)
    {            
        return $this->db->select("SELECT * FROM tran_web WHERE cuenta = :accountId AND numero_de_empresa  = :companyId ORDER BY seq_tran_web LIMIT 15",
        array('accountId' => $accountId,
               'companyId' => $companyId));
    }
}