<?php

class Statements_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function AccountDetail($accountId, $companyId)
    {
        return $this->db->select("SELECT * FROM saldos_web WHERE numero_cuenta = :accountId AND numero_de_empresa  = :companyId",
        array('accountId' => $accountId,
               'companyId' => $companyId));
    }
    
    public function LastMovements($accountId, $companyId)
    {
        return $this->db->select("SELECT * FROM tran_web WHERE cuenta = :accountId AND numero_de_empresa  = :companyId ORDER BY seq_tran_web LIMIT 15",
        array('accountId' => $accountId,
               'companyId' => $companyId));
    }

        public function ServersSingleList($Serversid)
    {
        return $this->db->select('SELECT * FROM servers WHERE serverid = :Serversid', 
            array('Serversid' => $Serversid));
    }
    
    public function create($data)
    {
        $this->db->insert('servers', array(
            'title' => $data['title'],
            'ip' => $data['ip'],
            'macaddress' => $data['macaddress'],
            'servicio' => $data['servicio'],
            'date_added' => date('Y-m-d H:i:s') // use GMT aka UTC 0:00
        ));
    }
    
    public function editSave($data)
    {
        $postData = array(
            'title' => $data['title'],
            'content' => $data['content'],
        );
        
        $this->db->update('servers', $postData, 
                "`Serversid` = '{$data['Serversid']}' AND userid = '{$_SESSION['userid']}'");
    }
    
    public function delete($id)
    {
        $this->db->delete('servers', "`serverid` = {$id}");
    }
}