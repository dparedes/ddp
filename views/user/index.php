<h1>Usuarios</h1>
<script>
	$(function(){
		$(".ajax").colorbox({width:"40%", height:"40%"});
		$(".add").colorbox({width:"40%", height:"40%"});
	});
	
</script>

<a class="add" href="<?php echo URL .'user/add';?>">Agregar nuevo Usuario</a>
<hr />
<div class="div-table">
	<div class="div-table-row">
		<div class="div-table-col2">ID</div>
		<div class="div-table-col">Usuario</div>
		<div class="div-table-col">Rol</div>
		<div class="div-table-col">Acci&oacute;n</div>
	</div>
	<?php foreach($this->userList as $key => $value) :?>
		<div class="div-table-row">
			<div class="div-table-col2"><?=$value['N_USUARIO'];?></div>
			<div class="div-table-col"><?=$value['E_MAIL'];?></div>
			<div class="div-table-col"><?=$value['No_EMPRESA'];?></div>
			<div class="div-table-col">
				<a class="ajax"  href="<?php echo URL .'user/edit/'.$value['N_USUARIO'];?>">Editar</a> | 
				<a class="delete" href="<?php echo URL .'user/delete/'.$value['N_USUARIO'];?>">Borrar</a> 
			</div>
		
		</div>
	<?php endforeach;?>
	
</div>
<script>
$(function() {
    
    $('.delete').click(function(e) {
        var c = confirm("Esta seguro de eleminiar el registro?");
        if (c == false) return false;
    });
    
});
</script>

