<h2>User: Edit</h2>
<?php
//print_r($this->user);


?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form method="post" action="<?php echo URL;?>user/editSave/<?php echo $this->user[0]['userid']; ?>">
                <label>Login</label><input type="text" name="login" value="<?php echo $this->user[0]['login']; ?>" /><br />
                <label>Password</label><input type="password" name="password" /><br />
                <label>Role</label>
                    <select name="role">
                        <option value="default" <?php if($this->user[0]['role'] == 'default') echo 'selected'; ?>>Default</option>
                        <option value="admin" <?php if($this->user[0]['role'] == 'admin') echo 'selected'; ?>>Admin</option>
            			<?php if (Session::get('role') == 'owner'):?>
            			<option value="owner">Owner</option>
            			<?php endif;?>
                    </select><br />
                <label>&nbsp;</label><input type="submit" />
            </form>
        </div>  
    </div>
</div>