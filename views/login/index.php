<div align="center"><h1>Ingreso al sistema</h1><br /></div>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
             <form method="post" action="login/run">
                 <div class="form-group">
                     <input type="email" class="form-control"  placeholder="correo@domain.com" name="login" required="" 
                            pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" />
                 </div>
                 <div class="form-group">
                     <input type="password" class="form-control" placeholder="Clave" name="password" required=""  />
                 </div>
                 <div class="form-group">
                     <div class="col-xs-offset-2 col-xs-10">
                         <button type="submit" class="btn btn-primary">Ingresar</button>
                     </div>
                 </div>
            </form>    
        </div>  
    </div>
        <?php if (Session::get('error') == 'error'): Session::destroy();?>
            <div class="alert alert-danger" id="error" align="center" style="margin-top:15px;">Por favor, Verifique sus datos</div>
        <?php elseif (Session::get('error') == 'empty'): Session::destroy();?>
            <div class="alert alert-danger" id="error" align="center" style="margin-top:15px;">Ning&uacute;n campo puede estar en blanco.</div>
        <?php endif;?>
</div>
