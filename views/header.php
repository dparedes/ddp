<!doctype html>
<html>
<head>
    <title><?=(isset($this->title)) ? $this->title : 'MVC'; ?></title>
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css" /> 
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/colorbox.css" /> 
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/sunny/jquery-ui.css" />
    <link href="<?php echo URL; ?>public/images/world.ico" type="image/x-icon" rel="shortcut icon" />
        <!-- Custom CSS -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/default-custom.css" rel="stylesheet" type="text/css" id="style_color"/>
    <!-- Fin Custom -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>public/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>public/js/jquery.colorbox.js"></script>
    <script>
	$(function(){
		$(".ajax").colorbox({width:"40%", height:"40%"});
		$(".password").colorbox({width:"60%", height:"60%"});
	});
	
    </script>
    <?php 
    if (isset($this->js)) 
    {
        foreach ($this->js as $js)
        {
            echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
        }
    }
    ?>
</head>
<body>
<?php Session::init(); ?>
<?php if (Session::get('loggedIn') == true):?>
  <div class="container">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar1" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active">
                  <a href="<?php echo URL; ?>dashboard">Dashboard</a>
                </li>
                <li>
                  <a href="#">Estado de cuenta</a>
                </li>
                <li>
                  <a class="password" href="<?php echo URL .'statements/edit/'.Session::get('accountId');?>">Actualizar clave</a>
                </li>
                <li>
                  <a href="<?php echo URL; ?>dashboard/logout">Salir <i class="icon-logout"></i></a>
                </li>               
              </ul>
              <div id="header">
                  <span id="welcome"> Hola: <?=Session::get('name')?></span>
              </div>            
         </div>
        <!--/.nav-collapse -->
      </div>
      <!--/.container-fluid -->
    </nav>
  </div>
<?php endif; ?>     
<div id="content">
    
    
    
    
