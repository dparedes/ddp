<script>
	$(function(){
		$(".ajax").colorbox({width:"40%", height:"40%"});
		$(".add").colorbox({width:"45%", height:"45%"});
	});
	
</script>
<h1>Servidores</h1>

<?php if (Session::get('error') == 'empty'): unset($_SESSION['error']);?>
	<div id="error">Ning&uacute;n campo puede estar en blanco.</div>
<?php endif;?>
<a class="add" href="<?php echo URL .'servers/add';?>">Agregar nuevo Servidor</a>
<hr />
<div class="div-table">
	<div class="div-table-row">
		<div class="div-table-col2">ID</div>
		<div class="div-table-col">Nombre</div>
		<div class="div-table-col">IP</div>
		<div class="div-table-col">MacAddress</div>
		<div class="div-table-col">Servicio</div>
		<div class="div-table-col">Fecha</div>
		<div class="div-table-col">Acci&oacute;n</div>
	</div>
	<?php foreach($this->ServersList as $key => $value) :?>
		<div class="div-table-row">
			<div class="div-table-col2"><?=$value['serverid'];?></div>
			<div class="div-table-col"><?=$value['title'];?></div>
			<div class="div-table-col"><?=$value['ip'];?></div>
			<div class="div-table-col"><?=$value['macaddress'];?></div>
			<div class="div-table-col"><?=$value['servicio'];?></div>
			<div class="div-table-col"><?=$value['date_added'];?></div>
			<div class="div-table-col">
				<a class="ajaxs"  href="<?php echo URL .'servers/edit/'.$value['serverid'];?>">Editar</a> | 
				<a class="delete" href="<?php echo URL .'servers/delete/'.$value['serverid'];?>">Borrar</a> 
			</div>
		
		</div>
	<?php endforeach;?>
	
</div>

<script>
$(function() {
    
    $('.delete').click(function(e) {
        var c = confirm("Esta seguro de eleminiar el registro?");
        if (c == false) return false;
    });
    
});
</script>