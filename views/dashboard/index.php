Bienvenido: <?=Session::get('name'). " " .Session::get('lastname');?>
<div>
                                    <!-- Begin: life time stats -->
    <div class="portlet box blue-steel">
        <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-thumb-tack"></i>Estado de cuenta: <?=Session::get('accountId');?>
          </div>
        </div>
        <div class="portlet-body">
          <div class="tabbable-line">
            <div class="tab-content">
             <div id="overview_1" class="tab-pane active">
                <div class="table-responsive">
                 <table class="table table-striped table-hover table-bordered">
                   <thead>
                      <tr>
                        <th>
                           N&uacute;mero de cuenta
                        </th>
                        
                        <th>
                           Fecha de emisi&oacute;n
                        </th>
                        
                        <th>
                            Saldo adeudado
                        </th>
                        
                        <th>
                           L&iacute;nea de cr&eacute;dito
                        </th>
                        
                        <th>
                           Cr&eacute;dito disponible
                        </th>
                        
                        <th>
                           Pago m&iacute;nimo
                        </th>
                      </tr>
                    </thead>
 <?php foreach($this->AccountDetail as $key => $value) :?>                   
                <tbody>
                  <tr>
                    <td>
                      <a><?=$value['NUMERO_CUENTA'];?></a>
                    </td>
                    
                    <td>
                      <a><?=$value['FECHA_ATUALIZACION'];?></a>
                    </td>
                    
                    <td>
                      <a><?=$value['SALDO_ADEUDADO'];?></a>
                    </td>
                    
                    <td>
                      <a><?=$value['LIMITE_CR'];?></a>
                    </td>
                    
                    <td>
                      <a><?=$value['SALDO_DISPONIBLE'];?></a>
                    </td>
                    
                    <td>
                      <a><?=$value['PAGO_MIN'];?></a>
                    </td>
                  </tr>
                </tbody>
 <?php endforeach;?>               
             </table>
            </div>
          </div>
         </div>
        </div>
       </div>
      </div>
 </div>
<!-- Ultimos movimientos -->
<div>
    <div class="portlet box blue-steel">
        <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-thumb-tack"></i>&Uacute;ltimos quince &#40;15&#41; movimientos: <?=Session::get('accountId');?>
          </div>
        </div>
        <div class="portlet-body">
          <div class="tabbable-line">
            <div class="tab-content">
             <div id="overview_1" class="tab-pane active">
                <div class="table-responsive">
                 <table class="table table-striped table-hover table-bordered">
                   <thead>
                      <tr>
                        <th>
                           Fecha
                        </th>
                        
                        <th>
                            Descripci&oacute;n
                        </th>
                        
                        <th>
                           Localidad
                        </th>
                        
                        <th>
                            Compras y d&eacute;bitos
                        </th>
                        
                        <th>
                            Pagos y cr&eacute;ditos
                        </th>                       
                      </tr>
                    </thead>
 <?php foreach($this->LastMovements as $key => $value) :?>                   
                <tbody>
                  <tr>
                    <td>
                      <a><?=$value['FECHA_TRAN'];?></a>
                    </td>
                    
                    <td>
                      <a><?=$value['DESCRIPCION'];?></a>
                    </td>
                    
                    <td>
                      <a><?=$value['LOCALIDA'];?></a>
                    </td>
                    
                    <td>
                      <a><?php if ($value['TIPO'] == 'DE') :?>
                         <?=$value['MONTO'];
                         endif;?>
                      </a>
                    </td>
                    
                    <td>
                      <a><?php if ($value['TIPO'] == 'CR') :?>
                         <?=$value['MONTO'];
                         endif;?>
                      </a>
                    </td>                   
                  </tr>
                </tbody>
 <?php endforeach;?>
             </table>
            </div>
          </div>
         </div>
        </div>
       </div>
      </div>
 </div>

<!-- Fin nuevos Datos -->
