<script>
	$(function(){
		$(".ajax").colorbox({width:"40%", height:"40%"});
		$(".add").colorbox({width:"45%", height:"45%"});
	});
	
</script>
<h1>Estado de cuenta de <?=Session::get('accountId');?>.</h1>

<?php if (Session::get('error') == 'empty'): unset($_SESSION['error']);?>
	<div id="error">Ning&uacute;n campo puede estar en blanco.</div>
<?php endif;?>
        <a class="add" href="<?php echo URL .'statements/download';?>">Descargado estado de cuenta</a>
<hr />
<div class="div-table">
	<div class="div-table-row">
            <div class="div-table-col2">N&uacute;mero de cuenta</div>
                <div class="div-table-col">Fecha de estado de cuenta</div>
                <div class="div-table-col">Saldo adeudado</div>
                <div class="div-table-col">L&iacute;nea de cr&eacute;dito</div>
                <div class="div-table-col">Cr&eacute;dito disponible</div>
                <div class="div-table-col">Pago m&iacute;nimo</div>
	</div>
	<?php foreach($this->AccountDetail as $key => $value) :?>
		<div class="div-table-row">
			<div class="div-table-col2"><?=$value['NUMERO_CUENTA'];?></div>
			<div class="div-table-col"><?=$value['FECHA_ULT_ESTADO_CTA'];?></div>
			<div class="div-table-col"><?=$value['SALDO_ADEUDADO'];?></div>
                        <div class="div-table-col"><?=$value['LIMITE_CR'];?></div>
			<div class="div-table-col"><?=$value['SALDO_DISPONIBLE'];?></div>
                        <div class="div-table-col"><?=$value['PAGO_MIN'];?></div>

		
		</div>
	<?php endforeach;?>
	
</div>
<br /><hr /><br />

<h1>&Uacute;ltimos quince &#40;15&#41; movimientos.</h1>

<hr />

<div class="div-table">
	<div class="div-table-row">
		<div class="div-table-col2">Fecha</div>
                <div class="div-table-col">Descripci&oacute;n</div>
                <div class="div-table-col">Localidad</div>
                <div class="div-table-col">Compras y D&eacute;bitos</div>
                <div class="div-table-col">Pagos y Cr&eacute;ditos</div>
	</div>
	<?php foreach($this->LastMovements as $key => $value) :?>
		<div class="div-table-row">
			<div class="div-table-col2"><?=$value['FECHA_TRAN'];?></div>
			<div class="div-table-col"><?=$value['DESCRIPCION'];?></div>
			<div class="div-table-col"><?=$value['LOCALIDA'];?></div>
			<div class="div-table-col"><?=$value['MONTO'];?></div>
                        <div class="div-table-col"><?=$value['SALDO'];?></div>

		</div>
	<?php endforeach;?>	
</div>



<script>
$(function() {
    
    $('.delete').click(function(e) {
        var c = confirm("Esta seguro de eleminiar el registro?");
        if (c == false) return false;
    });
    
});
</script>