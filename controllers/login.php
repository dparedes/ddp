<?php

class Login extends Controller {

    function __construct() {
        parent::__construct();    
    }
    
    function index() 
    {    
        $this->view->title = 'Login';
        
        $this->view->render('header');
        $this->view->render('login/index');
        $this->view->render('footer');
    }
    
    function run()
    {
		if (empty($_POST['login']) || empty($_POST['password'])){
			Session::init();
			Session::set('error', 'empty');
			header('location: ../login');
		}else
		{
			$this->model->run($_POST['login'],$_POST['password']);
		
		}
	}
}