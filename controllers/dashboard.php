<?php

class Dashboard extends Controller {

    function __construct() {
        parent::__construct();
        Auth::handleLogin();
        $this->view->js = array('dashboard/js/default.js');
    }
    
    function index() 
    {    
        $this->view->title = 'Dashboard';
        $this->view->AccountDetail = $this->model->AccountDetail(Session::get('accountId'),Session::get('companyId'));
        $this->view->LastMovements = $this->model->LastMovements(Session::get('accountId'),Session::get('companyId'));

        $this->view->render('header');
        $this->view->render('dashboard/index');
        $this->view->render('footer');
    }
    
    function logout()
    {
        Session::destroy();
        header('location: '.URL.'dashboard');
        exit;
    }
}