<?php

class Statements extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
    
    public function index() 
    {    
        $this->view->title = 'statements';
        $this->view->AccountDetail = $this->model->AccountDetail(Session::get('accountId'),Session::get('companyId'));
        $this->view->LastMovements = $this->model->LastMovements(Session::get('accountId'),Session::get('companyId'));
        
        $this->view->render('header');
        $this->view->render('statements/index');
        $this->view->render('footer');
    }
    
    public function add() {
        $this->view->render('servers/add');
    }
    
    public function create() 
    {
        
        if (empty($_POST['title']) || empty($_POST['ip']) || empty($_POST['macaddress']) ){
            
            Session::init();
            Session::set('error', 'empty');
            header('location: ' . URL . 'servers');
        }
        else
            {
            $data = array(
                'title' => $_POST['title'],
                'ip' => $_POST['ip'],
                'macaddress' => $_POST['macaddress'],
                'servicio' => $_POST['servicio']
            );
            $this->model->create($data);
            header('location: ' . URL . 'servers');
        }
    }
    
    public function edit($id) 
    {
        // $this->view->Servers = $this->model->ServersSingleList($id);
    
        // if (empty($this->view->Servers)) {
            // die('This is an invalid Servers!');
        // }
        
        // $this->view->title = 'Edit Servers';
        
        // $this->view->render('header');
        // $this->view->render('Servers/edit');
        // $this->view->render('footer');
        header('location: ' . URL . 'servers');
		
    }
    
    public function editSave($Serversid)
    {
        // $data = array(
            // 'Serversid' => $Serversid,
            // 'title' => $_POST['title'],
            // 'content' => $_POST['content']
        // );
        
        // // @TODO: Cheuquear error!
        
        // $this->model->editSave($data);
        // header('location: ' . URL . 'servers');
    }
    
    public function delete($id)
    {
        $this->model->delete($id);
        header('location: ' . URL . 'servers');
    }
}