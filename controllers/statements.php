<?php

class Statements extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
    
    public function index() 
    {    
        $this->view->title = 'statements';
        $this->view->AccountDetail = $this->model->AccountDetail(Session::get('accountId'),Session::get('companyId'));
        $this->view->LastMovements = $this->model->LastMovements(Session::get('accountId'),Session::get('companyId'));
        
        $this->view->render('header');
        $this->view->render('statements/index');
        $this->view->render('footer');
    }
    
    public function edit($accountId) {
        
        $this->view->title = 'Edit User';
        $this->view->edit = $this->model->userSingleList($accountId);
        $this->view->render('statements/edit');
    }
     public function editSave($accountId)
    {
        $data = array();
        $data['accountId'] = $accountId;
        $data['oldpassword'] = $_POST['oldpassword'];
        $data['newpassword1'] = $_POST['newpassword1'];
        $data['newpassword2'] = $_POST['newpassword2'];
        
       $this->model->editSave($data);
       header('location: ' . URL . 'dashboard');
        // @TODO: Do your error checking!
    }   

}